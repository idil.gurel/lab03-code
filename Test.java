package lab03;

public class Test {

	public static void main(String[] args) {
		Car car1= new Car("Mercedes", "Black", 3);
		Car car2= new Car("Ferrari", "Red", 4);
		Car car3= new Car("Tesla", "Blue", 2);
		
		car1.display();
		car2.display();
		car3.display();

		car1.drive(4, 80);
		car1.incrementGear();
		car1.display();
		System.out.println("Average speed of Car 1 is "+ car1.getAverageSpeed());
		car1.decrementGear();
        car1.decrementGear();
		
		car2.drive(2, 90);
		car2.incrementGear();
		car2.decrementGear();
		car2.incrementGear();
		car2.display();
		System.out.println("Average speed of Car 2 is "+ car2.getAverageSpeed());
		 car2.incrementGear();
	        car2.incrementGear();
	        car2.incrementGear();
	        car2.incrementGear();
		
		car3.drive(4.5, 70);
		car3.incrementGear();
		car3.incrementGear();
		car3.display();
		System.out.println("Average speed of Car 3 is "+ car3.getAverageSpeed());
		
	}

}