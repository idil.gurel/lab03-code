package lab03;

public class Car {
	private double odometer;
	private String brand;
	private String color;
	private int gear;
	private double totalNumberOfHoursDriven;

	public Car() {
		odometer = 0;
		brand= "Renault";
		color= "White";
		gear= 0;
		totalNumberOfHoursDriven= 0;
	}
	public Car(String b) {
		odometer=0;
		brand= b;
		color= "White";
		gear= 0;
		totalNumberOfHoursDriven= 0;
	}
	public Car(String b, String c) {
		odometer= 0;
		brand= b;
		color= c;
		gear= 0;
		totalNumberOfHoursDriven= 0;
	}
	public Car(int g) {
		odometer= 0;
		brand= "Renault";
		color= "White";
		gear= g;
		totalNumberOfHoursDriven= 0;
	}
	public Car(String b, String c, int g) {
		odometer=0;
		brand=b;
		color=c;
		gear=g;
		totalNumberOfHoursDriven= 0;
	}
	public void incrementGear() {
		if (gear < 5) {
            gear += 1;
        } else {
            System.out.println("Error: the gear value must be in range [0, 5].");
        }
	}
	public void decrementGear() {
		if (gear > 0) {
            gear -= 1;
        } else {
            System.out.println("Error: the gear value must be in range [0, 5].");
        }
	}
	public void drive(double HoursTravelled, double kmTravelledPerHour) { //x=t*V
		totalNumberOfHoursDriven += HoursTravelled;
		odometer += HoursTravelled*kmTravelledPerHour;
	}
	public double getOdometer() {
		return odometer;
	}
	public String getBrand() {
		return brand;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String c) {
		color= c;
	}
	public int getGear() {
		return gear;
	}
	public double getTotalNumberOfHoursDriven() {
		return totalNumberOfHoursDriven;
	}
	public void display() {
		System.out.println("The "+ brand+ " has color "+ color+ "."
	+ " It has travelled "+odometer+ " kms so far."
	+ " It is at gear "+ gear+ "."
	+ " The car has been driven for "+ totalNumberOfHoursDriven+ " hours.");
	}
	public double getAverageSpeed() {
		return odometer/totalNumberOfHoursDriven;
	}
	
	}
/*
=======
	public void display() {
		System.out.println("The "+ brand+ "has color "+ color+ "."
	+ " It has travelled "+odometer+ "kms so far."
	+ " It is at gear "+ gear+ ".");
	}
	}
/*7. Implement a Test class with a main method that creates three Car objects. Assign their
brand, color and gear. Then, call their display() methods to print their information.
8. Play with the code to do various things. Change the information of a car and call display() to
see the changes. 
BONUS:
>>>>>>> 9f32cc7259c77907e92d8f697e07f94f120f25e7
1. Add average speed to your Car class. Think about how to calculate the average speed. Hint:
You will need the total kms traveled (which is odometer). You may need to keep the total
number of hours the car was driven.
2. Add limit to the gear. Gear cannot be smaller than 0 or larger than 5. Print an error message
if incrementGear() or decrementGear() tries to set a value other than the defined range. */